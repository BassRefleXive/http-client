<?php

declare(strict_types=1);

namespace MBagrov\Http\Criteria;

interface UriCriteriaInterface
{
    public function path(): string;

    /**
     * @return string|null
     */
    public function param(string $param);

    public function query(): string;
}
