<?php

declare(strict_types=1);

namespace MBagrov\Http\Enum;

use MabeEnum\Enum;

/**
 * @method string getValue()
 */
final class Method extends Enum
{
    public const GET = 'GET';
    public const POST = 'POST';
    public const PUT = 'PUT';
    public const DELETE = 'DELETE';
}
