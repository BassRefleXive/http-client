<?php

declare(strict_types=1);

namespace MBagrov\Http\Response;

use MBagrov\Http\Exception\ResponseException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class XmlResponseParser
{
    private $encoder;

    public function __construct()
    {
        $this->encoder = new XmlEncoder();
    }

    public function responseArray(ResponseInterface $response): array
    {
        $data = $response->getBody()->getContents();

        return $this->encoder->decode($data, 'xml');
    }
}