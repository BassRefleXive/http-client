<?php

declare(strict_types=1);

namespace MBagrov\Http\Response;

use MBagrov\Http\Exception\ResponseException;
use Psr\Http\Message\ResponseInterface;

class JsonResponseParser
{
    public function responseArray(ResponseInterface $response): array
    {
        $data = $response->getBody()->getContents();

        if (null === $data = json_decode($data, true)) {
            throw ResponseException::invalidFormat('json');
        }

        return $data;
    }
}