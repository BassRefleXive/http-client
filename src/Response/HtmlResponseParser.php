<?php

declare(strict_types=1);

namespace MBagrov\Http\Response;

use Sky\Component\Core\Http\Exception\ResponseException;
use Symfony\Component\DomCrawler\Crawler;

class HtmlResponseParser
{
    final protected function extractNodeAttributeValue(Crawler $crawler, string $path, string $attribute): string
    {
        $node = $this->extractNode($crawler, $path);

        $key = $node->attr($attribute);

        if (null === $key) {
            throw ResponseException::nodeAttributeNotFound($path, $attribute);
        }

        return $key;
    }

    final protected function extractNode(Crawler $crawler, string $path): Crawler
    {
        $node = $crawler->filterXPath($path);

        $this->assertNodePresents($node, $path);

        return $node;
    }

    final protected function assertNodePresents(Crawler $node, string $path): void
    {
        if (!$node->count()) {
            throw ResponseException::nodeNotFound($path);
        }
    }
}