<?php

declare(strict_types=1);

namespace MBagrov\Http\Exception;


class ResponseException extends \RuntimeException
{
    public function __construct($message = '', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function invalidFormat(string $format): self
    {
        return new self(sprintf('Invalid response format. Expected "%s".', $format));
    }

    final public static function missingField(string $field, string $requiredFor): self
    {
        return new self(
            sprintf('Field "%s" required to create "%s" is missing in response.', $field, $requiredFor)
        );
    }
    final public static function missingHeader(string $header, string $requiredFor): self
    {
        return new self(
            sprintf('Header "%s" required to create "%s" is missing in response.', $header, $requiredFor)
        );
    }
}
