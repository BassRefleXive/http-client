<?php

declare(strict_types=1);

namespace MBagrov\Http\Exception;

class HttpClientException extends \RuntimeException
{
}
