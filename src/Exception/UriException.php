<?php

declare(strict_types=1);

namespace MBagrov\Http\Exception;

class UriException extends \RuntimeException
{
    public static function unresolvedEndpointPath(string $path): self
    {
        return new self(sprintf('Could not resolve "%s" path.', $path));
    }

    public static function missingPathParameter(string $path, string $param): self
    {
        return new self(sprintf('Could not set "%s" parameter of "%s" path.', $param, $path));
    }

    public static function invalidCriteriaType(string $expected, string $actual): self
    {
        return new self(sprintf('Invalid UriCriteria type. Expected %s actual %s.', $expected, $actual));
    }
}
