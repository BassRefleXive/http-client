<?php

declare(strict_types=1);

namespace MBagrov\Http;

use MBagrov\Http\Exception\HttpClientException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface HttpClientInterface
{
    /**
     * @throws HttpClientException
     */
    public function send(RequestInterface $request, array $options = []): ResponseInterface;
}
