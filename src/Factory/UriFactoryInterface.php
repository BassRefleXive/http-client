<?php

declare(strict_types=1);

namespace MBagrov\Http\Factory;

use MBagrov\Http\Criteria\UriCriteriaInterface;

interface UriFactoryInterface
{
    public function create(UriCriteriaInterface $uriCriteria): string;
}
