<?php

declare(strict_types=1);

namespace MBagrov\Http\RequestBody;

interface BodyParamsInterface
{
    public function params(): array;
}
