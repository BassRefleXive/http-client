<?php

declare(strict_types=1);

namespace MBagrov\Http\RequestBody\Factory;

use MBagrov\Http\RequestBody\BodyParamsInterface;

class UrlencodedBodyFactory extends RequestBodyFactory
{
    public function create(BodyParamsInterface $params): string
    {
        return urldecode(http_build_query($params->params()));
    }
}
