<?php

declare(strict_types=1);

namespace MBagrov\Http\RequestBody\Factory;

use MBagrov\Http\RequestBody\BodyParamsInterface;

class JsonBodyFactory extends RequestBodyFactory
{
    public function create(BodyParamsInterface $params): string
    {
        return json_encode($params->params());
    }
}
