<?php

declare(strict_types=1);

namespace MBagrov\Http\RequestBody\Factory;

use MBagrov\Http\RequestBody\BodyParamsInterface;
use GuzzleHttp\Psr7\MultipartStream;

abstract class RequestBodyFactory
{
    /**
     * @return MultipartStream|string
     */
    abstract public function create(BodyParamsInterface $params);
}
